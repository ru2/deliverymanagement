DELETE
FROM PRODUCTS
WHERE product_ui_id in (1, 2, 3, 4, 5);

ALTER TABLE Products
    MODIFY product_ui_id NVARCHAR2(40);

ALTER TABLE Products
    MODIFY product_name NOT NULL;

ALTER TABLE Products RENAME COLUMN product_ui_id to nomenclature_uuid;
