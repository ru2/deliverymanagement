ALTER TABLE PRODUCT_NOMENCLATURE DROP COLUMN PRODUCT_ID;
ALTER TABLE PALLETS DROP COLUMN PALLET_ID;

ALTER TABLE PRODUCTS DROP CONSTRAINT PROD_NOM_FK;
ALTER TABLE PRODUCTS DROP CONSTRAINT PROD_PAL_FK;

ALTER TABLE PRODUCT_NOMENCLATURE DROP COLUMN NOMENCLATURE_UUID;
ALTER TABLE PALLETS DROP COLUMN PALLET_UUID;

ALTER TABLE PRODUCT_NOMENCLATURE ADD NOMENCLATURE_UUID NVARCHAR2(40);
ALTER TABLE PRODUCT_NOMENCLATURE ADD CONSTRAINT NOM_PK PRIMARY KEY (NOMENCLATURE_UUID);

ALTER TABLE PALLETS ADD PALLET_UUID NVARCHAR2(40);
ALTER TABLE PALLETS ADD CONSTRAINT PAL_PK PRIMARY KEY (PALLET_UUID);

ALTER TABLE PRODUCTS
    ADD CONSTRAINT PROD_NOM_FK FOREIGN KEY (NOMENCLATURE_UUID)
        REFERENCES PRODUCT_NOMENCLATURE(NOMENCLATURE_UUID);
ALTER TABLE PRODUCTS
    ADD CONSTRAINT PROD_PAL_FK FOREIGN KEY (pallet_uuid)
        REFERENCES PALLETS(pallet_uuid);