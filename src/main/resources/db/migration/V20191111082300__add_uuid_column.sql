alter table pallets
    add pallet_uuid varchar(40);
    COMMENT ON COLUMN pallets.pallet_uuid IS 'This is UUID identifier for PALLETS table.';


alter table products
    add pallet_uuid varchar(40);
        COMMENT ON COLUMN products.pallet_uuid IS 'This is UUID identifier for PRODUCTS table.';
