delete from pallets where pallet_id in (
                                       1,
                                       2,
                                       3);

alter table PALLETS modify DIMENSION_WIDTH NUMBER (6, 2) ;

alter table PALLETS modify DIMENSION_HEIGHT NUMBER (6, 2) ;

alter table PALLETS modify DIMENSION_LENGTH NUMBER (6, 2) ;
