CREATE TABLE pallets
(
    pallet_id    NUMBER(6) PRIMARY KEY,
    pallet_weight NUMBER(8,2)   not null ,
    dimension_width NUMBER(6)       NOT NULL,
    dimension_height NUMBER(6)      NOT NULL,
    dimension_length NUMBER(6)      NOT NULL


);

CREATE SEQUENCE pallet_seq NOCACHE;
ALTER TABLE pallets
    MODIFY pallet_id DEFAULT pallet_seq.nextval;
insert into pallets (pallet_weight, DIMENSION_WIDTH, DIMENSION_HEIGHT, DIMENSION_LENGTH) VALUES (50,5,5,5);
insert into pallets (pallet_weight, DIMENSION_WIDTH, DIMENSION_HEIGHT, DIMENSION_LENGTH) VALUES (85, 7,3,2);
insert into pallets (pallet_weight, DIMENSION_WIDTH, DIMENSION_HEIGHT, DIMENSION_LENGTH) VALUES (17, 1,2,5);



