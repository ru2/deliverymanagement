package com.deliverymanagement.controllers;

import com.deliverymanagement.dto.ProductRequestDto;
import com.deliverymanagement.dto.ProductResponseDto;
import com.deliverymanagement.facade.ProductFacade;
import com.deliverymanagement.service.ProductService;
import com.deliverymanagement.utils.Transformers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
@Api(value = "product", description = "CRUD operations for product", tags = { "PRODUCT" })
public class ProductsController {

    private final ProductFacade productFacade;

    @GetMapping
    @ApiOperation(value = "GET ALL PRODUCTS", notes = "\n" +
            "This function shows all the products")
    public List<ProductResponseDto> getAllProducts() {
        return productFacade.findAll();
    }

    @PutMapping("/{productUuid}")
    @ApiOperation(value = "UPDATE THE PRODUCT WITH UUID", notes = "\n" +
            "This function updates the products whit specified uuid")
    public ProductResponseDto updateProduct(@RequestBody ProductRequestDto product, @PathVariable String productUuid) {
        return productFacade.updateProduct(productUuid, product);
    }

    @DeleteMapping("/{productUuid}")
    @ApiOperation(value = "DELETE THE PRODUCT WITH UUID", notes = "\n" +
            "This function deletes the product whit specified uuid")
    public ProductResponseDto deleteProduct(@PathVariable String productUuid) {
        return productFacade.deleteProduct(productUuid);
    }

    @GetMapping("/{productUuid}")
    @ApiOperation(value = "FIND THE PRODUCT WITH UUID", notes = "\n" +
            "This function finds the products whit specified uuid")
    public ProductResponseDto findByProductByUuid(@PathVariable String productUuid) {
        return productFacade.findProductByUuid(productUuid);
    }
}
