package com.deliverymanagement.controllers;


import com.deliverymanagement.dto.PalletRequestDto;
import com.deliverymanagement.dto.PalletResponseDto;
import com.deliverymanagement.dto.ProductResponseDto;
import com.deliverymanagement.entities.Pallet;
import com.deliverymanagement.facade.PalletFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pallets")
@RequiredArgsConstructor
@Api(value = "pallet", description = "CRUD operations for pallet", tags = { "PALLET" })

public class PalletsController {

    private final PalletFacade palletFacade;

    @PostMapping
    @ApiOperation(value = "CREATE A PALLET", notes = "\n" +
            "This function creates the pallet")
    public Pallet createPallet(@RequestBody PalletRequestDto palletRequestDto) {
        return palletFacade.createPallet(palletRequestDto);
    }

    @GetMapping
    @ApiOperation(value = "GET ALL PALLETS", notes = "\n" +
            "This function gets all the pallets")
    public List<PalletResponseDto> getAllPallets() {
        return palletFacade.findAll();
    }

    @GetMapping("/{palletUUID}")
    @ApiOperation(value = "FIND A PALLET WITH UUID", notes = "\n" +
            "This function finds the pallet")
    public PalletResponseDto findByUUID(@PathVariable String palletUUID) {
        return palletFacade.findByUUID(palletUUID);
    }

    @PutMapping("/{palletUUID}")
    @ApiOperation(value = "UPDATE A PALLET", notes = "\n" +
            "This function updates the pallet")
    public PalletResponseDto updatePallet(@RequestBody PalletRequestDto palletRequestDto, @PathVariable String palletUUID) {
        return palletFacade.updatePallet(palletRequestDto, palletUUID);
    }

    @DeleteMapping("/{palletUUID}")
    @ApiOperation(value = "DELETE A PALLET", notes = "\n" +
            "This function deletes the pallet")
    public PalletResponseDto deletePallet(@PathVariable String palletUUID) {
        return palletFacade.deletePallet(palletUUID);
    }

    @GetMapping("/{palletUUID}/products")
    @ApiOperation(value = "FIND ALL PALLET PRODUCTS", notes = "\n" +
            "This function finds all the pallet products")
    public List<ProductResponseDto> findAllPalletProducts(@PathVariable String palletUUID) {
        return palletFacade.findAllPalletProducts(palletUUID);
    }

    @PutMapping("/{pallet_uuid}/products/{removed_product_uuid}/{added_product_uuid}")
    @ApiOperation(value = "UPDATE PRODUCT ON THEPALLET", notes = "\n" +
            "This function updates the product on a pallet")
    public ProductResponseDto updateProductOnPallet(@PathVariable("pallet_uuid") String palletUUID, @PathVariable("removed_product_uuid") String removedProductUuid, @PathVariable("added_product_uuid") String addedProductUuid) {
        return palletFacade.updateProductOnPallet(palletUUID, removedProductUuid, addedProductUuid);
    }

    @PostMapping("/{pallet_uuid}/products/{product_uuid}")
    @ApiOperation(value = "ADD THE PRODUCT ON THE PALLET", notes = "\n" +
            "This function adds the product on a pallet")
    public ProductResponseDto addProductOnPallet(@PathVariable("pallet_uuid") String palletUUID, @PathVariable("product_uuid") String productUuid) {
        return palletFacade.addProductOnPallet(palletUUID, productUuid);
    }

    @DeleteMapping("/products/{product_uuid}")
    @ApiOperation(value = "DELETE THE PRODUCT FROM THE PALLET ", notes = "\n" +
            "This function deletes the product on the pallet")
    public ProductResponseDto deleteProductFromPallet(@PathVariable("product_uuid") String productUuId) {
        return palletFacade.deleteProductFromPallet(productUuId);
    }

    @DeleteMapping("/{pallet_uuid}/products")
    @ApiOperation(value = "DELETE ALL PRODUCTS FROM THE PALLET", notes = "\n" +
            "This function deletes all the products from the pallet")
    public List<ProductResponseDto> deleteAllFromPallet(@PathVariable("pallet_uuid") String palletUuid) {
        return palletFacade.deleteAllFromPallet(palletUuid);
    }
}