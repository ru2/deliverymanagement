package com.deliverymanagement.controllers;

import com.deliverymanagement.dto.ProductNomenclatureRequestDto;
import com.deliverymanagement.dto.ProductNomenclatureResponseDto;
import com.deliverymanagement.dto.ProductRequestDto;
import com.deliverymanagement.dto.ProductResponseDto;
import com.deliverymanagement.facade.NomenclatureFacade;
import com.deliverymanagement.facade.ProductFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/nomenclature")
@RequiredArgsConstructor
@Api(value = "nomenculature", description = "CRUD operations for nomenclature", tags = { "NOMENCLATURE" })
public class ProductsNomenclatureController {

    private final NomenclatureFacade nomenclatureFacade;
    private final ProductFacade productFacade;

    @PostMapping
    @ApiOperation(value = "SAVE THE NOMENCLATURE", notes = "\n" +
            "This function saves the nomenclature")
    public ProductNomenclatureResponseDto saveNomenclature(@RequestBody ProductNomenclatureRequestDto productNomenclatureRequestDto) {
        return nomenclatureFacade.saveProduct(productNomenclatureRequestDto);
    }

    @GetMapping
    @ApiOperation(value = "GET ALL NOMENCLATURE", notes = "\n" +
            "This function gets all nomenclature")
    public List<ProductNomenclatureResponseDto> getAllNomenclatures() {
        return nomenclatureFacade.findAll();
    }

    @PutMapping("/{nomenclatureUuid}")
    @ApiOperation(value = "UPDATE THE NOMENCLATURE", notes = "\n" +
            "This function updates the nomenclature")
    public ProductNomenclatureResponseDto updateNomenclature(@RequestBody ProductNomenclatureRequestDto productNomenclatureRequestDto, @PathVariable String nomenclatureUuid) {
        return nomenclatureFacade.updateProduct(productNomenclatureRequestDto, nomenclatureUuid);
    }

    @DeleteMapping("/{nomenclatureUuid}")
    @ApiOperation(value = "DELETE THE NOMENCLATURE", notes = "\n" +
            "This function deletes the nomenclature")
    public ProductNomenclatureResponseDto deleteNomenclature(@PathVariable String nomenclatureUuid) {
        return nomenclatureFacade.deleteProduct(nomenclatureUuid);
    }

    @GetMapping("/{nomenclatureUuid}")
    @ApiOperation(value = "FIND THE NOMENCLATURE WITH UUID", notes = "\n" +
            "This function finds the nomenclature with")
    public ProductNomenclatureResponseDto findByNomenclatureUuid(@PathVariable String nomenclatureUuid) {
        return nomenclatureFacade.findByProductUuid(nomenclatureUuid);
    }

    @PostMapping("/{nomenclatureUuid}/product")
    @ApiOperation(value = "CREATE THE PRODUCT", notes = "\n" +
            "This function creates the nomenclature")
    public ProductResponseDto createProduct(@PathVariable String nomenclatureUuid, @RequestBody ProductRequestDto productRequestDto){
        return productFacade.createProduct(nomenclatureUuid, productRequestDto);
    }
}