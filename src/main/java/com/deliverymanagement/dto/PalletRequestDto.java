package com.deliverymanagement.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@Builder
public class PalletRequestDto {
   private final Double palletWeight;
   private final Double dimensionWidth;
   private final Double dimensionHeight;
   private final Double dimensionLength;

   @Override
   public String toString() {
      return "Pallet {" +
              "\npalletWeight=" + palletWeight +
              ",\n dimensionWidth=" + dimensionWidth +
              ",\n dimensionHeight=" + dimensionHeight +
              ",\n dimensionLength=" + dimensionLength +
              '}';
   }
}