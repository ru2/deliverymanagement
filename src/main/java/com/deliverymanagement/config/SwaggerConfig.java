package com.deliverymanagement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket automaticApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaData());
    }
    private ApiInfo metaData() {
        Contact contact = new Contact("Delivery Management", "", "");

        return new ApiInfo(
                "Delivery Management Documentation",
                "This swagger documentation is automatically generated for DELIVERY MANAGEMENT API",
                "1.0.0",
                "for educational purposes only",
                contact,
                "",
                "",
                Collections.emptyList());
    }

}
