package com.deliverymanagement.exceptions;

import com.deliverymanagement.utils.ExceptionType;

public class ApplicationException extends RuntimeException {

   private final ExceptionType exceptionType;

    public ExceptionType getExceptionType() {
        return exceptionType;
    }

    public ApplicationException(ExceptionType exceptionType) {
        this.exceptionType = exceptionType;
    }

    public ApplicationException(String message, ExceptionType exceptionType) {
        super(message);
        this.exceptionType = exceptionType;
    }

    public ApplicationException(String message, Throwable cause, ExceptionType exceptionType) {
        super(message, cause);
        this.exceptionType = exceptionType;
    }

    public ApplicationException(Throwable cause, ExceptionType exceptionType) {
        super(cause);
        this.exceptionType = exceptionType;
    }

    public ApplicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ExceptionType exceptionType) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.exceptionType = exceptionType;
    }
}
