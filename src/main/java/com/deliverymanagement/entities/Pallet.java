package com.deliverymanagement.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "pallets")
@ApiModel(description = "Details about the pallets")
public class Pallet implements Serializable {

    @Column(name = "pallet_weight")
    @ApiModelProperty(notes = "The pallet's weight")
    private Double palletWeight;

    @Column(name = "dimension_width")
    @ApiModelProperty(notes = "The pallet's wight")
    private Double dimensionWidth;

    @Column(name = "dimension_height")
    @ApiModelProperty(notes = "The pallet's height")
    private Double dimensionHeight;

    @Column(name = "dimension_length")
    @ApiModelProperty(notes = "The pallet's dimension lenght")
    private Double dimensionLength;

    @Id
    @Column(name = "pallet_uuid")
    @ApiModelProperty(notes = "The pallet's uuid")
    private String palletUUID;
}
