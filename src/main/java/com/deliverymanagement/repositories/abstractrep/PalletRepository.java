package com.deliverymanagement.repositories.abstractrep;

import com.deliverymanagement.entities.Pallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PalletRepository extends JpaRepository<Pallet, String> {
}
