package com.deliverymanagement.repositories.abstractrep;

import com.deliverymanagement.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findAllByPalletPalletUUID(String palletUuid);
}
