package com.deliverymanagement.integration;


import com.deliverymanagement.entities.Pallet;
import com.deliverymanagement.repositories.abstractrep.PalletRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.util.UriComponentsBuilder.fromUri;
import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PalletsIntegrationTest {

    private final URI palletPath;
    private final UriComponentsBuilder palletPathVariable;
    private final JdbcTemplate jdbcTemplate;
    private PalletRepository palletRepository = null;


    @Autowired
    private MockMvc mockMvc;


    public PalletsIntegrationTest(@Autowired PalletRepository palletRepository,
                                  @Autowired JdbcTemplate jdbcTemplate,
                                  @LocalServerPort int port) {
        final String localhost = "http://localhost";
        final String path = "/pallets";
        palletPath = fromUriString(localhost).port(port).path(path).build().toUri();
        palletPathVariable = fromUri(palletPath).path("/{palletUUID}");

        this.palletRepository = palletRepository;
        this.jdbcTemplate = jdbcTemplate;
    }


    @BeforeEach
    public void init() {
        Pallet inputPallet = new Pallet(10.0, 10.0, 50.0, 10.0, "1");
        when(palletRepository.findById("1")).thenReturn(Optional.of(inputPallet));
    }

    @Test
    public void findByIdIsOK(String palletUUID) throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/pallets/{id}", 1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(
                        MockMvcResultMatchers.jsonPath("$palletUUID").value(1));
    }

}
