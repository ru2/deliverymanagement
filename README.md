# Delivery Management Application

This application source code can only be used for educational purposes. 

## Table of Contents

1. [ Description ](#desc)
2. [ Installation ](#setup)


## 1. Description
This project was developed during the internship with our team. The main goal was to create a prototype for delivery management applications for different products by distributing them by pallets. The final user should be able to manage Products as well as Pallets (containers for products) adding, removing and updating them.

Stack used in this project was Spring, Spring Boot, Spring Data, Hibernate, JDBC, Swagger, Flyway migration, Maven.

My role in this project was to develop Priduct-per-Pallet functionality using hibernate mapping, creating repositories for additional functionality, rest controllers to check pallets and products using REST. As well as JUnit test for this application.


## 2. Installation
The project requires Apache Maven to be installed on the machine.

* Please use this article to Create and Configure a Container Database (CDB) in Oracle Database 12c 
https://oracle-base.com/articles/12c/multitenant-create-and-configure-container-database-12cr1
* Create user - DBADMIN and password - admin
* Use Flyway plugin to migrate data from resources